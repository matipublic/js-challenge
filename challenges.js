/* Ejercicio 1 : LenghtEncoding */

var string = "AAAAAAAAAAAAABBCCCCDD"

var lengthEncoding = function (str) {
    var arr = str.split('')

    return arr.reduce((prev, curr, index) => {
        var split = prev.split('')
        var num = split[split.length-2]
        if(prev === curr) {
            if(num) {
                return num+1+""+curr
            } else {
                return '2' + curr
            }
        } else {

        }
        return prev+curr
    }, "")
}

/* Ejercicio 2 : Sorted Squared Array */

var array =  [-10, -5, 0, 5, 10]

var sortedSquaredArray = function (arr) {
    var result = arr.map((val) => val*val)
    return result.sort((a,b) => a-b)
}